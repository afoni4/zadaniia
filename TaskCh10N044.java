package HomeWork;

public class TaskCh10N044 {
    static int digroot( int n){

        int root = 0;

        while ( n > 0 || root > 9) {

            if (n == 0) {
                n = root;

                root = 0;
            }
            root += n % 10;
            n /= 10;
        }return root;
    }

    public static void main(String[] args) {
        int n = 65785412;
        System.out.println(digroot(n));
    }



}

