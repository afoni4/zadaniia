import java.util.Scanner;
public class TaskCh10N055 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Insert number ");
        int number = s.nextInt();
        System.out.println("Insert Base");
        int n = s.nextInt();
        System.out.println(convertFromDecimalToBaseN(number, n, 1));
    }
    public static Integer convertFromDecimalToBaseN(int num, int n, int pow) {
        Integer r = num % n;

        if (num < n)
            return new Double((Math.pow(10, pow - 1)) * r.doubleValue()).intValue();

        return convertFromDecimalToBaseN(num / n, n, pow + 1) +
                new Double(Math.pow(10, pow - 1) * r.doubleValue()).intValue();
    }
}