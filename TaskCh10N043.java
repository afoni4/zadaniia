package HomeWork;

public class TaskCh10N043 {

    static int sumnumber(int n) {
        if (n / 10 != 0) return n;
        return n % 10 + sumnumber( n / 10);

    }
    static int number (int n){
        if (0 != n / 10) return n;
        return number( n /10) + 1;

    }

    public static void main(String[] args) {
      System.out.println(sumnumber(10));
      System.out.println(number(10));
    }



}

