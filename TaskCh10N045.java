package HomeWork;

public class TaskCh10N045 {

    static int progression(int memb, int diff, int n) {
        if (n > 0)
            memb = diff + progression(memb, diff, n - 1);
        return memb;
    }
    static int SumProgression(int sum, int diff, int n) {
        if (n > 0)
            sum = sum + (n * diff) +SumProgression(sum, diff, n - 1);
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(progression(3, 7, 1));
        System.out.println(SumProgression(3, 7, 1));


    }
}