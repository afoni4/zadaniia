public class TaskCh06N080 {
    public static void main(String[] args) {
        int  num = 90909090;
        int count = 0;

        while ( num != 0) {
            if (num % 10 == 0)
                count++;
            else if ( num % 10 == 9)
                count--;

            num = num / 10;
        }
        if ( count > 0)
            System.out.println(" 0 Встречается чаще");
        else if (count < 0)
            System.out.println(" 9 Встречается чаще");
        else System.out.println(" 0 И 9 Одинаковое количество");


    }
}
