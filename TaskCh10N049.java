public class TaskCh10N049 {
    static int IndexOfMax(int arr[], int len)
    {
        if (len == 0) return len;
        int i = IndexOfMax(arr, len -1);
        return arr[len] > arr[i] ? len : i;
    }

    public static void main(String[] args) {
        int num [] = {1,123,4,1,2,3,1,3,45,66,3,43,};
        System.out.println(IndexOfMax(num,num.length - 1));
    }
}
