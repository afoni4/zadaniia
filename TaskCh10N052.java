public class TaskCh10N052 {
    public static int reversDigits(int num) {
        if (num < 1) {
            return 0;
        }
        int temp = num % 10;
        num = (num - temp) / 10;
        System.out.println(temp);

        return reversDigits(num);
    }

    public static void main(String[] args) {
        reversDigits(10);
    }


}