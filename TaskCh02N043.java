import java.util.Scanner;

public class Task02N043 {
    public static void main(String[] args) { 
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();

        int n = (a / b ) * (b / a );

        System.out.println(n);
    }
}
