public class TaskCh10N048 {

    static int MaxElement(int arr[], int size) {
        if (size > 0)
            return Math.max(arr[size - 1], MaxElement(arr, size - 1));
        return arr[0];
    }

    public static void main(String[] args) {
        int num[] = {19,-12,12,435,653,221,1};
        int size = num.length;
        System.out.println(MaxElement(num,size));
    }
}