import java.util.Arrays;

public class TaskCh11N158 {
    public static void main(String[] args) {
        int a[] = new int[5];
        for (int i = 0; i < 5;i++){
            a[i] = i;
        }
        System.out.println(Arrays.toString(a));
        System.out.println();
        for (int i = 0; i<a.length-1; i++){
            for (int j=i+1; j<a.length-1;j++){
                if (a[i]==a[j]) {
                    int k = a[i];
                    a[j] = a[j+1];
                    a[i] = k;
                    a[a.length - i] = 0;
                }
            }
            System.out.println(Arrays.toString(a));
        }
    }
}

