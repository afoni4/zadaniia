public class TaskCh03N029 {
    public static void main(String[] args) {

        int x = 1,y = 11,z = 3;
        boolean a = x % 2 != 0 && y % 2 !=0;
        System.out.println(a);

        x = 2; y = 30;
        boolean b = x < 20 && y >= 20 || x >= 20 && y < 20;
        System.out.println(b);

        x = 0; y = 5;
        boolean c = x == 0 || y == 0;
        System.out.println(c);

        x = -2; y = -1; z = -5;
        boolean g = x < 0 && y < 0 && z < 0;
        System.out.println(g);

        x = -2; y = -1; z = -5;
        boolean d = x % 5 ==0 && y % 5 != 0 && z % 5 !=0 || x % 5 != 0 && y % 5 == 0 && z % 5 != 0
                     || x % 5 != 0 && y % 5 != 0 && z % 5 == 0;
        System.out.println(d);

        x = -2; y = -1; z = 101;
        boolean e = x > 100 || y > 100 || z > 100;
        System.out.println(e);

    }
}

