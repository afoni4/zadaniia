import java.util.Scanner;

public class TaskCh10N053 {
    public static int consoleInputFirstNumber() {
        Scanner scan = new Scanner(System. in );
        System.out.print("Enter number: ");
        if (scan.hasNextInt()) {
            return scan.nextInt();
        } else {
            System.out.println("Entered not number. Try again.");
            return consoleInputFirstNumber();
        }
    }

    public static void numberReverseOrder(int number) {
        if (number == 0) {
            return;
        } else {
            System.out.print(number % 10);
            numberReverseOrder(number / 10);
        }
    }
    public static void main(String[] args) {
        int input = consoleInputFirstNumber();
        numberReverseOrder(input);
    }
}

