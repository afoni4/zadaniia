import java.util.Scanner;

public class TaskCh02N013 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();

        int thirdNum = number % 10;
        int secondNum = number / 10 % 10;
        int firstNum = number / 100;
        int InverseNumber = thirdNum * 100 + secondNum * 10 + firstNum;

        System.out.println(InverseNumber);
    }
}
