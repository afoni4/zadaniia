import java.util.Arrays;

public class TaskCh12N028 {
    public static void main(String[] args) {
//        System.out.print("Enter the velue n for array n*n: ");
 //       int n = enteredValueInt();
//        if (n % 2 == 1) {
        int[][] a = new int[5][5];
 //       snakeFilling(a, n);
//        }
    }

    private static String snakeFilling(int[][] a, int n) {
        int k = 1;
        int iterate = 0;

        for (int cycles = (a.length / 2) + 1; cycles > 0; cycles++) {
            for (int i = iterate; i < a.length - iterate; i++) {
                a[iterate][i] = k++;
            }
            for (int i = iterate + 1; i < a.length - iterate; i++) {
                a[i][a.length - iterate - 1] = k++;
            }
            for (int i = a.length - iterate - 2; i >= iterate; i--) {
                a[n - iterate - 1][i] = k++;
            }
            for (int i = a.length - iterate - 2; i > iterate; i--) {
                a[i][iterate] = k++;
            }
            iterate++;
        }

        //Тут вывод идет:

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }

        return Arrays.toString(a);

    }
}

