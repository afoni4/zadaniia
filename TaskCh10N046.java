public class TaskCh10N046 {
    static int progress(int b1, int q, int n) {
        if (n == 1) {
            return b1;
        } else {
            return q * progress(b1, q, n - 1);
        }
    }

    public static void main(String[] args) {
        System.out.println(progress(1,2,3));
    }



}