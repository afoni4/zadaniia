public class TaskCh10N041 {
    int fact ( int n) {
        int result ;

        if (n==1) return 1;
        result = fact( n - 1) * n;
        return result;
    }
}
class Recursion {
    public static void main(String[] args) {
        TaskCh10N041 f = new TaskCh10N041();

        System.out.println(f.fact(3));

    }

}
