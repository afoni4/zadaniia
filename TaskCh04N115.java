import java.util.Scanner;

public class TaskCh04N115 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int year = input.nextInt();

        int odd = year - 1984;
        String colour;

        switch (odd % 12) {
            case 0:
                System.out.println("rat");
                break;
            case 1:
                System.out.println("cow");
                break;
            case 2:
                System.out.println("pig");
                break;
            case 3:
                System.out.println("tiger");
                break;
            case 4:
                System.out.print("dog");
                break;
            case 5:
                System.out.print("cok");
                break;
            case 6:
                System.out.print("monkey");
                break;
            case 7:
                System.out.print("sheep");
                break;
            case 8:
                System.out.print("horce");
                break;
            case 9:
                System.out.print("dragon");
                break;
            case 10:
                System.out.print("bunny");
                break;
            case 11:
                System.out.print("snake");
                break;
        }

        switch (odd % 10) {
            case 0:
            case 1:
                System.out.print("green");
                break;
            case 2:
            case 3:
                System.out.print("black");
                break;
            case 4:
            case 5:
                System.out.print("red");
                break;
            case 6:
            case 7:
                System.out.print("white");
                break;
            case 8:
            case 9:
                System.out.print("yellow");
                break;
        }

    }

}
