import java.util.Arrays;

public class TaskCh12N025 {

        public static void main(String[] args) {
//            System.out.print("Enter the number of rows in the array: ");
//            int rows = enteredValueInt();
//            System.out.print("Enter the number of columns  in the array: ");
//            int columns = enteredValueInt();
            int a[][] = new int[12][10];
            int b[][] = new int[10][12];

            System.out.println("Result of the example a:");
            exampleA(a);
            System.out.println("\nResult of the example b:");
            exampleB(a);
            System.out.println("\nResult of the example c:");
            exampleC(b);
        }

        private static String exampleA(int[][] a) {
            int k = 1;
            for (int i = 0; i < 12; i++) {
                for (int j = 0; j < 10; j++) {
                    a[i][j] = k++;
                }
            }
            for (int i = 0; i < 12; i++) {
                for (int j = 0; j < 10; j++) {
                    System.out.print(a[i][j] + " ");
                }
                System.out.println();
            }
            return Arrays.toString(a);
        }

        private static String exampleB(int[][] a) {
            int k = 1;
            for (int j = 0; j < 10; j++) {
                for (int i = 0; i < 12; i++) {
                    a[i][j] = k++;
                }
            }
            for (int i = 0; i < 12; i++) {
                for (int j = 0; j < 10; j++) {
                    System.out.print(a[i][j] + " ");
                }
                System.out.println();
            }
            return Arrays.toString(a);
        }

        private static String exampleC(int[][] a) {
            int k = 120;
            for (int i = 12; i > 0; i--) {
                for (int j = 0; j < 10; j++) {
                    a[i][j] = k--;
                }
            }
            for (int i = 0; i < 12; i++) {
                for (int j = 0; j < 10; j++) {
                    System.out.print(a[i][j] + " ");
                }
                System.out.println();
            }
            return Arrays.toString(a);
        }

    }
