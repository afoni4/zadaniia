import java.util.Arrays;
public class TaskCh12N023 {
    public static void main(String[] args) {
        System.out.print("Enter the number of rows in the array: ");
        int rows = enteredValueInt();
        System.out.print("Enter the number of columns  in the array: ");
        int columns = enteredValueInt();
        int a[][] = new int[rows][columns];
        System.out.println("Result of the example a:");
        exampleA(a);
        System.out.println("\nResult of the example b:");
        exampleB(a);
        System.out.println("Result of the example c:");
        exampleC(a);
    }

    private static String exampleA(int[][] a) {
        for (int i = 0; i<a.length;i++){
            for (int j = 0; j <a.length;j++){
                if (i == j || i+j==a.length-1) {
                    a[i][j] = 1;
                }else
                    a[i][j] = 0;
            }
        }

        for (int i = 0; i<a.length;i++){
            for (int j = 0; j <a.length;j++){
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        return Arrays.toString(a);
    }

    private static String exampleB(int[][] a) {
        for (int i = 0; i<a.length;i++){
            for (int j = 0; j <a.length;j++){
                if (i == j || i+j==a.length-1|| j == (a.length-1)/2 || i == (a.length-1)/2) {
                    a[i][j] = 1;
                }else
                    a[i][j] = 0;
            }
        }

        for (int i = 0; i<a.length;i++){
            for (int j = 0; j <a.length;j++){
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        return Arrays.toString(a);
    }

    private static String exampleC(int[][] a) {
        for (int i = 0; i<a.length/2+1;i++){
            for (int j = 0; j <a[i].length-i;j++){
                if ((j<i)||(j>=(a[i].length-i))) {
                    a[i][j] = 0;
                }else
                    a[i][j] = 1;
            }
        }

        for (int i=a.length-1; i>=a.length/2; i--){
            for (int j=0; j<a[i].length; j++){
                if ((j<(a[i].length-1-i)) || (j>i))
                    a[i][j]=0;
                else
                    a[i][j]=1;}
        }

        for (int i = 0; i<a.length;i++){
            for (int j = 0; j <a.length;j++){
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        return Arrays.toString(a);
    }