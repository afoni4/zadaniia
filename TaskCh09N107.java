public class TaskCh09N107 {
    public static void main(String[] args) {

        String word = "anaconda";
        char[] charWord = word.toCharArray();
        int i;
        int a = -1;
        int o = -1;
        char bufA = 'a';
        char bufO = 'o';
        for (i = 0; i < charWord.length; i++) {
            switch (charWord[i]) {
                case 'a':
                    if (a == -1) {
                        bufA = charWord[i];
                        a = i;
                    }
                    break;
                case 'o':
                    bufO = charWord[i];
                    o = i;
                    break;
            }
        }
        charWord[o] = bufA;
        charWord[a] = bufO;
        String s = new String(charWord);
        System.out.println(s);

    }

}

