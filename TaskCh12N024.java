import java.util.Arrays;

public class TaskCh12N024 {

        public static void main(String[] args) {
//        System.out.print("Enter the number of rows in the array: ");
//        int rows = enteredValueInt();
//        System.out.print("Enter the number of columns  in the array: ");
//        int columns = enteredValueInt();
            int a[][] = new int[6][6];
            System.out.println("Result of the example a:");
            exampleA(a);
//        System.out.println("\nResult of the example b:");
//        exampleB(a);
        }

        private static String exampleA(int[][] a) {
            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < a.length; j++) {
                    if (i == 0 || j == 0) {
                        a[i][j] = 1;
                    } else if (i == 1 && j > 0) {
                        a[i][j] = j + 1;
                    } else if (j == 1 && i > 0) {
                        a[i][j] = i + 1;
                    } else if (i > 1) {
                        a[i][j] = a[i - 1][j] + a[i][j - 1];
                    }
                }
            }
            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < a.length; j++) {
                    System.out.print(a[i][j] + " ");
                }
                System.out.println();
            }
            return Arrays.toString(a);

        }
    }
