import java.util.Scanner;

public class TaskCh04N036 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int time = input.nextInt();

        if (time % 5 <= 3 ){
            System.out.println("red");
        }else
            System.out.println("green");
    }
}

