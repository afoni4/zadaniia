import java.util.ArrayList;
import java.util.List;

public class TaskCh13N012 {
    public static void main(String[] args) {
        int year = 0;
// List<Employee> a1 = new ArrayList<>();
        TaskCh13N012 task = new TaskCh13N012();
        Database db = new Database();

// task.listFilling(a1);
        int month;
        System.out.print("Вывод стажа до n лет(кнопка 1), Вывода стажа всех сотрудников(кн.2), Поиск(кн.3): ");
        int selectNum = enteredValueInt();
        if (selectNum == 1) {
            System.out.print("Введите месяц для сравнения:");
            month = enteredValueInt();
            if (month > 0 && month < 13) {
                System.out.print("Введите год для сравнения:");
                year = enteredValueInt();
                System.out.print("Не менее скольки лет должен быть стаж сотрудника: ");
                int experience = enteredValueInt();
                System.out.println("\nСПИСОК СОТРУДНИКОВ , У КОТОРЫХ СТАЖ НЕ МЕНЕЕ " +
                        experience + " ЛЕТ НА " + month + "." + year);

//                experienceCalc(a1, month, year, experience);
                experienceCalc(db.listFilling(db.a1), month, year, experience);
            } else {
                System.out.println("Некорректно введен месяц ");
            }
        } else if (selectNum == 2) {
            System.out.print("\nВведите месяц для вывода общего стажа сотрудников:");
            month = enteredValueInt();
            if (month > 0 && month < 13) {
                System.out.print("Введите год для вывода общего стажа сотрудников:");
                year = enteredValueInt();
                System.out.println("\nКОЛИЧЕСТВО ПРОРАБОТАННЫХ ЛЕТ ВСЕХ СОТРУДНИКОВ НА " +
                        month + "." + year + ":");
//                experienceCalcAll(a1, month, year);
                experienceCalcAll(db.listFilling(db.a1), month, year);
            } else {
                System.out.println("Месяц введен некорректно");
            }
        } else if (selectNum == 3) {
            System.out.print("Введите строку для поиска: ");
            String s = enteredValueString();
//            new Database().searchEmployees(a1, s);
            new Database().searchEmployees(db.listFilling(db.a1), s);

        }
    }

//    protected List<Employee> listFilling(List<Employee> a1) {
//        a1.add(new Employee("Иванов", "Иван", "Иванович", "Пушкина 8", 1, 1995));
//        a1.add(new Employee("Петров", "Семен", "Колотушкина 9", 2, 1996));
//        a1.add(new Employee("Альба", "Джессика", "Петровна", "7 микрорайон", 3, 1997));
//        a1.add(new Employee("Гимадеев", "Нияз", "Рустамович", "Ленинский просп. 5", 4, 1998));
//        a1.add(new Employee("Сидоров", "Владимир", "Сергеевич", "Ахунова 6", 5, 1999));
//        a1.add(new Employee("Дроздов", "Андрей", "Иванович", "29 комплекс", 6, 2000));
//        a1.add(new Employee("Воля", "Павел", "Николаевич", "5 авеню", 7, 2001));
//        a1.add(new Employee("Егоров", "Андрей", "Юрьевич", "Сахарова, 32", 8, 2002));
//        a1.add(new Employee("Гилев", "Кирилл", "Семенович", "Пушкина 8", 9, 2003));
//        a1.add(new Employee("Пушкин", "Александр", "Сергеевич", "7 микрорайон", 10, 2000));
//        a1.add(new Employee("Лермонтов", "Михаил", "Юрьевич", "Ленинский просп. 5", 11, 2001));
//        a1.add(new Employee("Мартин", "Люттер", "Кинг", "Ахунова 6", 12, 2002));
//        a1.add(new Employee("Суворов", "Александр", "Васильевич", "29 комплекс", 1, 2003));
//        a1.add(new Employee("Давыдов", "Денис","5 авеню", 2, 2004));
//        a1.add(new Employee("Ленин", "Владимир", "Ильич", "Сахарова, 32", 3, 2005));
//        a1.add(new Employee("Сталин", "Иосиф", "Виссарионович", "Кирова 76", 4, 2006));
//        a1.add(new Employee("Маркс", "Карл", "Камала 93", 5, 2007));
//        a1.add(new Employee("Ковалевская", "Софья", "Королева 100", 6, 2008));
//        a1.add(new Employee("Куликова", "Любовь", "Ивановна", "Глушко 47", 7, 2009));
//        a1.add(new Employee("Драгунова", "Мария", "Андреевна", "Зинина 4", 8, 2010));
//        return a1;
//    }

    private static List<Employee> experienceCalc(List<Employee> a1, int month, int year, int experience) {
        for (int i = 0; i < a1.size(); i++) {
            if ((month >= a1.get(i).getMonth() & year - a1.get(i).getYear() >= experience) ||
                    year - a1.get(i).getYear() > experience) {
                System.out.print(i + 1 + " ");
                System.out.print(a1.get(i).getSurname() + " ");
                System.out.print(a1.get(i).getName() + " ");
                System.out.print(a1.get(i).getPatronymic() + " ");
                System.out.print("|" + a1.get(i).getAddress() + "| ");
                System.out.print(a1.get(i).getMonth() + " ");
                System.out.print(a1.get(i).getYear() + " ");
                System.out.println();
            }
        }
        return a1;
    }

    private static List<Employee> experienceCalcAll(List<Employee> a1, int month, int year) {
        int expYear = 0;
        int experience;
        for (int i = 0; i < a1.size(); i++) {
            experience = 0;
            System.out.print(i + 1 + " ");
            System.out.print(a1.get(i).getSurname() + " ");
            System.out.print(a1.get(i).getName() + " ");
            System.out.print(a1.get(i).getPatronymic() + " ");
            System.out.print("|" + a1.get(i).getAddress() + "| ");
            System.out.print(a1.get(i).getMonth() + " ");
            System.out.print(a1.get(i).getYear() + " ");
            if (month > a1.get(i).getMonth()) {
                experience = -1;
            }
            expYear = (year - a1.get(i).getYear()) + experience;
            System.out.print("Стаж работы: " + expYear + " лет");
            System.out.println();
        }
        return a1;
    }
}

class Employee {
    private String surname;
    private String name;
    private String patronymic;
    private String address;
    private int month;
    private int year;

    Employee(String surn, String n, String patronym, String addr, int mm, int yyyy) {
        surname = surn;
        name = n;
        patronymic = patronym;
        address = addr;
        month = mm;
        year = yyyy;
    }

    Employee(String surn, String n, String addr, int mm, int yyyy) {
        this(surn, n, "", addr, mm, yyyy);
    }

    protected String getSurname() {
        return surname;
    }

    protected void setSurname(String surname) {
        this.surname = surname;
    }

    protected String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected String getPatronymic() {
        return patronymic;
    }

    protected void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    protected String getAddress() {
        return address;
    }

    protected void setAddress(String address) {
        this.address = address;
    }

    protected int getMonth() {
        return month;
    }

    protected void setMonth(int month) {
        this.month = month;
    }

    protected int getYear() {
        return year;
    }

    protected void setYear(int year) {
        this.year = year;
    }
}

class Database {
    public static List<Employee> searchEmployees(List<Employee> a1, String s) {
        System.out.println("\nСписок сотрудников");
        for (int i = 0; i < a1.size(); i++) {
            if (a1.get(i).getName().toUpperCase().contains(s.toUpperCase()) ||
                    a1.get(i).getPatronymic().toUpperCase().contains(s.toUpperCase())) {
                System.out.print(i + 1 + " ");
                System.out.print(a1.get(i).getSurname() + " ");
                System.out.print(a1.get(i).getName() + " ");
                System.out.print(a1.get(i).getPatronymic() + " ");
                System.out.print("|" + a1.get(i).getAddress() + "| ");
                System.out.print(a1.get(i).getMonth() + " ");
                System.out.print(a1.get(i).getYear() + " ");
                System.out.println();
            }
        }
        return a1;
    }

    List<Employee> a1 = new ArrayList<>();

    protected List<Employee> listFilling(List<Employee> a1) {
        a1.add(new Employee("Иванов", "Иван", "Иванович", "Пушкина 8", 1, 1995));
        a1.add(new Employee("Петров", "Семен", "Колотушкина 9", 2, 1996));
        a1.add(new Employee("Альба", "Джессика", "Петровна", "7 микрорайон", 3, 1997));
        a1.add(new Employee("Гимадеев", "Нияз", "Рустамович", "Ленинский просп. 5", 4, 1998));
        a1.add(new Employee("Сидоров", "Владимир", "Сергеевич", "Ахунова 6", 5, 1999));
        a1.add(new Employee("Дроздов", "Андрей", "Иванович", "29 комплекс", 6, 2000));
        a1.add(new Employee("Воля", "Павел", "Николаевич", "5 авеню", 7, 2001));
        a1.add(new Employee("Егоров", "Андрей", "Юрьевич", "Сахарова, 32", 8, 2002));
        a1.add(new Employee("Гилев", "Кирилл", "Семенович", "Пушкина 8", 9, 2003));
        a1.add(new Employee("Пушкин", "Александр", "Сергеевич", "7 микрорайон", 10, 2000));
        a1.add(new Employee("Лермонтов", "Михаил", "Юрьевич", "Ленинский просп. 5", 11, 2001));
        a1.add(new Employee("Мартин", "Люттер", "Кинг", "Ахунова 6", 12, 2002));
        a1.add(new Employee("Суворов", "Александр", "Васильевич", "29 комплекс", 1, 2003));
        a1.add(new Employee("Давыдов", "Денис", "5 авеню", 2, 2004));
        a1.add(new Employee("Ленин", "Владимир", "Ильич", "Сахарова, 32", 3, 2005));
        a1.add(new Employee("Сталин", "Иосиф", "Виссарионович", "Кирова 76", 4, 2006));
        a1.add(new Employee("Маркс", "Карл", "Камала 93", 5, 2007));
        a1.add(new Employee("Ковалевская", "Софья", "Королева 100", 6, 2008));
        a1.add(new Employee("Куликова", "Любовь", "Ивановна", "Глушко 47", 7, 2009));
        a1.add(new Employee("Драгунова", "Мария", "Андреевна", "Зинина 4", 8, 2010));
        return a1;
    }
}
