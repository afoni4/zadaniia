import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();
        
        int thirdNum = number % 10;
        int secondNum = number / 10 % 10;
        int firstNum = number / 100;
        int n = firstNum * 100 + thirdNum * 10 + secondNum;

        System.out.println(n);
    }
}
