import java.util.Scanner;

public class TaskCh04N033 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num = input.nextInt();

        if (num % 2 == 0){
            System.out.println(true);
        }else
            System.out.println(false);
    }
}
